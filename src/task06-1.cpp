#include "Circle.h"
#include <iostream>

using namespace std;

int main()
{
	double cost_road, cost_wall;
	Circle pool(3.0);
	Circle wall(4.0);

	cost_road = (wall.get_area() - pool.get_area()) * 1000;
	cost_wall = wall.get_fer() * 2000;

	cout << "Answer: " << endl;
	cout << "Cost track: " << cost_road << " rub." << endl;
	cout << "Cost wall: " << cost_wall << " rub." << endl;
	return 0;
}
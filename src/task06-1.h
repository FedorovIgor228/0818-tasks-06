#include <cmath>
#define pi 3.1415926535

class Circle
{
private:
	double radius, ference, area;
public:
	Circle(double rad)
	{
		radius = rad;
		ference = 2 * pi * radius;
		area = pi * radius * radius;
	}
	void new_rad(double rad)
	{
		if (rad > 0) {
			radius = rad;
			ference = 2 * pi * radius;
			area = pi * radius * radius;
		}
		else
			cout << "radius<=0"<<endl ;
	}
	void new_ference(double fer)
	{
		if (fer > 0) {
			ference = fer;
			radius = ference / (2 * pi);
			area = pi * radius * radius;
		}
		else
			cout << "ference<=0" << endl;
	}
	void new_area(double sqre)
	{
		if (sqre > 0) {
			area = sqre;
			radius = sqrt(area / pi);
			ference = 2 * pi * radius;
		}
		else
			cout << "area<=0" << endl;

	}

	double get_rad()
	{ return radius; }
	double get_fer() 
	{ return ference; }
	double get_area() 
	{ return area; }
};

